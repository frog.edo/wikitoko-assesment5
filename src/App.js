import "./App.css";
import React from "react-router-dom";
import NavbarComponents from "./components/NavbarComponent.jsx";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import CheckoutComponent from "./components/CheckoutComponent";
import HomeComponent from "./components/HomeComponent";

function App() {
  return (
    <>
      <Router className="App">
        <NavbarComponents />
        <Routes>
          <Route path="/" element={<HomeComponent />} />
          <Route path="/checkout" element={<CheckoutComponent />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
