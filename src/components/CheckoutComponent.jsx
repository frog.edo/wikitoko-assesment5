import { useSelector } from "react-redux";
import CartComponent from "./CartComponent";

function CheckoutComponent() {
  const cart = useSelector((data) => data.CartReducer.cart);
  console.log("cart: ", cart);

  return (
    <div className="py-5 my-5">
      {cart.map((item) => {
        return (
          <div
            key={item.uid}
            className="container d-flex justify-content-center"
            style={{ maxWidth: "600px" }}
          >
            <div
              className="card m-3 w-100 shadow-sm"
              style={{ height: "200px" }}
            >
              <div
                key={item.uid}
                className="row g-0 px-2 d-flex align-items-center h-100"
              >
                <div className="col-md-4">
                  <img
                    src={item.image.url}
                    className="rounded-start product-img w-100"
                    alt={item.image.alt}
                    style={{ height: "180px" }}
                  />
                </div>
                <div className="col-md-8">
                  <div className="card-body">
                    <h5 className="card-title">{item.productName}</h5>
                    <p className="card-text">
                      <small className="text-muted">
                        {`${item.qty} x Rp ${parseInt(
                          item.price
                        ).toLocaleString()},-`}
                      </small>
                    </p>
                    <p className="card-text">{`Rp ${parseInt(
                      item.qty * item.price
                    ).toLocaleString()},-`}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      })}
      <CartComponent
        button="Payment"
        link="/"
        total={cart.map((obj) => obj.qty * obj.price).reduce((a, b) => a + b)}
      />
    </div>
  );
}

export default CheckoutComponent;
