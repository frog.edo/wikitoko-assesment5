import { Link } from "react-router-dom";

const NavbarComponents = () => {
  return (
    <>
      <nav className="navbar navbar-light bg-body fixed-top shadow-sm">
        <div className="container-lg">
          <Link to="/" className="navbar-brand">
            WikiToko
          </Link>
        </div>
      </nav>
    </>
  );
};

export default NavbarComponents;
