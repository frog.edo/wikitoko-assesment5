import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getListItem } from "../redux/action/CartAction";
import LoadingComponent from "./LoadingComponent";
import ProductComponent from "./ProductComponent";

const HomeComponent = () => {
  const { loading, error, listItem } = useSelector(
    (state) => state.CartReducer
  );
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getListItem());
  }, [dispatch]);

  return (
    <>
      {listItem && !error ? (
        <ProductComponent listItem={listItem} />
      ) : loading && !error ? (
        <LoadingComponent />
      ) : (
        error && <div>error 404 not found</div>
      )}
    </>
  );
};

export default HomeComponent;
